# styleguide

This is a vue application that consumes the generic Styleguide of Peerlancers web apps.

This also serve as a one stop shop for all style related concerns on peerlancers web development.

INSTALLATION
- run `npm install` in node-js server

WATCH STYLE CHANGES
- run `npm run watch:scss` in node-js server

BUILD STYLE CHANGES
- run `npm run build:scss` in node-js server

OVERRIDE DEFAULT SETUP:
- on your global styling i.e `global.scss`, set the root context based on the following setup

``` global.scss
:root {
  // Fonts
  --pl-font-family: 'PT Sans', sans-serif !default;
  --pl-font-path: "/dist/fonts" !default;

  --pl-font-size-xs: 10;
  --pl-font-size-sm: 11;
  --pl-font-size-md: 13;
  --pl-font-size-lg: 15;
  --pl-font-size-xl: 20;

  // Spaces
  --pl-space-xs: 5px;
  --pl-space-sm: 10px;
  --pl-space-md: 15px;
  --pl-space-lg: 20px;
  --pl-space-xl: 30px;

  // Colors
  --pl-color-primary:  #66FCF1 !default;
  --pl-color-secondary: #1F2833 !default;
  --pl-color-tertiary: #38424E !default;
  --pl-color-success: #3d9400 !default;
  --pl-color-warning: #ddd939 !default;
  --pl-color-danger: #dd4b39 !default;
  --pl-color-light: #fafafb !default;
  --pl-color-medium: #656B73 !default;
  --pl-color-dark: #0E141d !default;
}
```